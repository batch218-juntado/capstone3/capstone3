import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import './App.css';

export default function App() {
    return (
      // Common pattern in react.js for a component to return multiple elements
      <>
      <UserProvider value={{user, setUser, unsetUser}}>
      {/*Initializes the dynamic routing*/}
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />

            <Route path="/*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
      </UserProvider>
      </>
    );
  };
