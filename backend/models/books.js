const mongoose = require("mongoose");

const bookSchema = new mongoose.Schema({
	name:{
		type: String,
		// Required
		/*
			The "true" value defines if the field is required or not and the second element in the array 
			is the message that will be printed out in our terminal when the data is not present.
		*/
		required: [true, "Book is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true	
	},
	createdOn: {
		type: Date,
		/*
			The "new Date()" expression instantiates a new "date" of the current date and time 
			whenever a course is created in our database
		*/
	default: new Date()
	},
	
	Orders: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date ()
			}
		}
	]

});
								// model name
module.exports = mongoose.model("Book", bookSchema);