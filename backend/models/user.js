const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required."]
	},
	password : {
		type : String,
		required : [true, "Password is required."]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile Number is required."]
	},
	// The "purchases" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
	purchases : [
		{
			bookId : {
				type : String,
				required : [true, "Book ID is required"]
			},
			boughtOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Purchased"
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);
