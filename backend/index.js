/*
	Gitbash:
	npm init -y
	npm install express
	npm install mongoose
	npm install cors
	npm install bcrypt
	npm install jsonwebtoken
*/

// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Routers
const userRoutes = require("./routes/userRoute.js")
const bookRoutes = require("./routes/bookRoute.js");

// to create an express server/application
const app = express();

// Middlewares
app.use(cors());
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended: true}));

// Initializing the routes
app.use("/users", userRoutes);
app.use("/books", bookRoutes);

// Connect to MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@ecommerce.pq5lgwg.mongodb.net/?retryWrites=true&w=majority",
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompts a message once you are conencted.
mongoose.connection.once('open', () => console.log('Now connected to Hong-Mongo DB Atlas.'));

